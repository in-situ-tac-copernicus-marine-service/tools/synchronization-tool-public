# Changelog

All notable changes to this project will be documented in this file.


## [3.2.0] - 2024-11-28

### Added
- The remote listing can now be done either with the CMT API or with direct S3 URLs requests 
- RemoteListingMethod option in the configuration file is added
- tools compatible with copernicusmarine API versions 1.X and 2.X

### Deleted
- NoRemoteListing option in the configuration file is removed

### Changed
- Update the versions of python packages 
- Better management of Coriolis logs (ending and cleaning) (Ifremer only)
- Update documentation (README). Espacially RemoteListing parts


## [3.1.3] - 2024-11-04

### Changed
- Use copernicusmarine toolbox version 1.3.4 instead of 1.2.3
- Update documentation (README). Add aiohttp issue


## [3.1.2] - 2024-10-04

### Changed
- Update documentation (README). Functional workflow and description in case of dataset exists on DV but not on MDS added.


## [3.1.1] - 2024-05-23

### Changed
- Use copernicusmarine toolbox version 1.2.3 instead of 1.3.4
- Update ifremer internal scripts


## [3.1.0] - 2024-04-11

### Changed
- Use copernicusmarine toolbox version 1.1.0 instead of 1.0.5

### Added
- Check if index files can be decoded/opened before get date of update and before pushing the file
- Delete old index tmp files (older than 7 days) if not properly deleted by the script


## [3.0.0] - 2024-03-22

- First push/version in this project