#!/usr/bin/env python
import os
import datetime
import hashlib
import ftplib
import re
import csv
from time import time, gmtime, strftime, mktime
from copy import copy
from itertools import repeat
from pathos.multiprocessing import ProcessingPool as Pool
from mds_insitu_sync.FileSync import LocalFileSync, RemoteFileSync
from pathlib import Path
from xml.etree import ElementTree as ET
from xml.dom import minidom
from netCDF4 import Dataset


def read_index_file(index_file_path):
    # TODO : Not Used
    """
    Read ascii file and convert into python variables
    :param index_file_path: Index ascii file
    :return: raw_list
    """
    raw_list = []
    index_file = []
    with open(index_file_path, 'r') as i:
        index_file = i.readlines()
    # First, we will search for the csv header line position
    header_pos = 0
    for idx, line in enumerate(index_file):
        if not line.startswith('#'):
            header_pos = idx - 1
            break
    # And we reduce the list to the header+contents
    index_file = index_file[header_pos:]
    # Now, extract csv content with header to a list of dict :
    reader = csv.DictReader(index_file)
    raw_list = [r for r in reader]
    return raw_list


def list_subdirs_from_files(files):
    """
    Create a list of directories available from a list of files objects.
    Each directory is unique and the list is sorted by len and by alphabetical order
    :param files: List of files(FileSync)
    :return: List of directories
    """
    dirs = set()

    for file in files:
        path = Path(file.relative_path)
        while path != path.parent:
            if not path.parent == Path("."):
                dirs.add(str(path.parent))
            path = path.parent

    dirs_list = sorted(dirs, key=lambda x: (len(x), x))
    return dirs_list


def list_local_files(conf):
    """
    if conf.listingMethod is set to "files" mode :
      list all files in sourceDatasetPath or sourceDatasetPath/datasetPart folder
    else :
      not implemented yet
    :param conf: (SyncConfiguration) instance
    :return: List(LocalFileSync)
    """
    files_list = []
    src = Path(conf.dataset["sourceDatasetPath"]) / conf.dataset["datasetPart"]

    if conf.dataset["listingMethod"] == "file":
        for path in src.rglob('*'):
            if path.is_file():
                if conf.dataset["excludeRegex"] != "" and re.match(conf.dataset["excludeRegex"], path.name):
                    continue
                f = LocalFileSync(path.name, str(path.relative_to(conf.dataset["sourceDatasetPath"])), str(path))
                files_list.append(f)
    else:
        # TODO waiting index files management
        pass
        """
        raw_list = read_index_file(conf.dataset["sourceIndexDataFile"])
        # close file & build list of LocalFileSync :
        for f in raw_list:
            lf = from_csv_to_file_sync(conf, f, file_location="local")
            if lf is not None:
                files_list.append(lf)
        """

    # TODO Warning file is not necessary a netcdf fille
    if conf.dataset["dateUpdateFromNetcdf"]:
        conf.cmems_logger.log_info("Will look for date_update attribute in netCDF files.")
        with Pool(20) as pool:
            files_list_result = pool.map(set_local_file_last_update_timestamp, files_list)
        files_list = files_list_result

    return files_list


def set_local_file_last_update_timestamp(local_file):
    """
    For a given local netCDF file, this method gets its "date_update" attribute and sets the local_file object last_motidification_time attribute.
    :param local_file: LocalFileSync
    :return: local_file LocalFileSync
    """
    f = Dataset(local_file.absolute_path, "r")
    local_file.last_modification_timestamp = int(mktime(datetime.datetime.strptime(f.date_update, "%Y-%m-%dT%H:%M:%SZ").timetuple()))
    f.close()
    return local_file


def find_donwloaded_mds_listing_files(conf):
    """
    Find all potential remote listing files
    :param conf: (SyncConfiguration) instance
    :return: List of pattern files found
    """
    if conf.dataset["datasetPart"]:
        file_pattern = f'{conf.dataset["datasetId"]}_{conf.dataset["datasetVersion"]}_{conf.dataset["datasetPart"]}*.txt'
    else:
        file_pattern = f'{conf.dataset["datasetId"]}_{conf.dataset["datasetVersion"]}*.txt'

    return Path(conf.transfer["tmpCopyPath"]).glob(file_pattern)


def delete_downloaded_mds_listing_files(conf):
    """
    Search and delete all temporary remote listing files
    :param conf: (SyncConfiguration) instance
    :return: None
    """

    # Create a list of files matching the pattern
    files_to_delete = find_donwloaded_mds_listing_files(conf)

    # Delete each file in the list
    for file_path in files_to_delete:
        try:
            file_path.unlink()
            conf.cmems_logger.log_debug(f"Deleted file: {file_path}")
        except OSError as e:
            conf.cmems_logger.log_error_and_exit(f"Error deleting file: {file_path} - {str(e)}")


def list_remote_files(conf, cmems_mds, timestamp):
    """
    Download remote file list of the specified product/dataset/part
    :param conf: (SyncConfiguration) instance
    :param cmems_mds: mds object
    :param timestamp: timestamp of the current process
    :return: remote_files_list - List(RemoteFileSync)
    """
    remote_files_list = []
    if conf.dataset["listingMethod"] == "file":
        try:
            if conf.dataset["datasetPart"]:
                output_file = f'{conf.dataset["datasetId"]}_{conf.dataset["datasetVersion"]}_{conf.dataset["datasetPart"]}_{timestamp}.txt'
            else:
                output_file = f'{conf.dataset["datasetId"]}_{conf.dataset["datasetVersion"]}_{timestamp}.txt'

            delete_downloaded_mds_listing_files(conf)

            if conf.mds["remoteListingMethod"] == "copernicusmarine_api":
                cmems_mds.list_remote_files_with_copernicusmarine_api(output_file)
            elif conf.mds["remoteListingMethod"] == "direct_s3":
                cmems_mds.list_remote_files_with_custom_method(output_file)

            # Open file and extract files
            listing_mds_files = list(find_donwloaded_mds_listing_files(conf))

            fixed_part = f"{conf.dataset['datasetId']}_{conf.dataset['datasetVersion']}"
            if len(listing_mds_files) == 1:
                with open(listing_mds_files[0], 'r') as f:
                    for line in f:
                        line = line.strip()
                        filename = Path(line).name
                        relative = line.split(fixed_part)[1].lstrip("/")
                        absolute = line
                        rf = RemoteFileSync(filename, relative, absolute)
                        remote_files_list.append(rf)
            else:
                conf.cmems_logger.log_error_and_exit("The number of files found is different from 1 in {}".format(Path(conf.transfer["tmpCopyPath"])))

            delete_downloaded_mds_listing_files(conf)

        except Exception as e:
            conf.cmems_logger.log_error_and_exit(f"Listing remote files failed :: error : {str(e)}")
    else:
        pass
        # TODO waiting index files management

    return remote_files_list


def read_timestamp_file(conf):
    """
    Reads the timestamp file given in configuration file and returns the  timestamp that it contains.
    :param conf: (SyncConfiguration) instance
    :return: None
    """
    error_msg1 = "You must set the reference timestamp in file {}".format(conf.dataset["timestampFile"])
    error_msg2 = 'echo `date --date="1971-01-01 00:00:00" +%s` > {}'.format(conf.dataset["timestampFile"])
    try:
        with open(conf.dataset["timestampFile"], 'r') as f:
            raw_timestamp = f.readline().rsplit('\n')[0]
        if re.match("^-?\d+$", raw_timestamp):
            return int(raw_timestamp)
        else:
            conf.cmems_logger.log_error(error_msg1)
            conf.cmems_logger.log_error_and_exit(error_msg2)
    except (OSError, IOError) as e:
        conf.cmems_logger.log_error(str(e))
        conf.cmems_logger.log_error(error_msg1)
        conf.cmems_logger.log_error_and_exit(error_msg2)


def write_timestamp_file(conf, new_timestamp):
    """
    Replace old value in Timestamp file by the timestamp of the job start datetime
    :param conf: (SyncConfiguration) instance
    :param new_timestamp: New timestamp to use
    :return:
    """
    with open(conf.dataset["timestampFile"], 'w') as f:
        f.write(str(new_timestamp))


def create_remote_directories(conf, directories, cmems_ftp):
    """
    Create FTP needed folders
    :param conf: (SyncConfiguration) instance
    :param directories: list of directories to create
    :param cmems_ftp: FTP instance that is already cwd to the destination dir
    :return: None
    """
    dirs_list = sorted(directories, key=lambda file: (os.path.dirname(file), os.path.basename(file)))
    ftp_conn = cmems_ftp.get_write_ftp_conn()
    for new_subdir in dirs_list:
        try:
            ftp_conn.mkd(new_subdir)
        except ftplib.error_perm as ep:
            if str(ep) == "550 Create directory operation failed.":
                # This means that the directory already exists on FTP side. No worries.
                pass
            else:
                ftp_conn.close()
                error_msg = "An error occured trying to create directory {} on FTP side. Exiting.".format(new_subdir)
                conf.cmems_logger.log_error_and_exit(error_msg)


def upload_files_list(conf, files_list, cmems_ftp):
    if conf.transfer["parallelTransfer"]:
        with Pool(conf.transfer["parallelTransferCount"]) as pool:
            files_list_result = pool.map(upload_single_file, files_list, repeat(conf), repeat(cmems_ftp))
        files_list = files_list_result
    else:
        dataset_size = len(files_list)
        dataset_cursor = 1
        for f in files_list:
            while f.number_of_attempts < conf.transfer["sendingAttempts"]:
                if f.final_status == "Delivered" or f.final_status == "Cancelled":
                    break
                conf.cmems_logger.log_info("Uploading {} : {} / {}".format(f.relative_path, dataset_cursor, dataset_size))
                f = upload_single_file(f, conf, cmems_ftp)
            dataset_cursor += 1

    return files_list 


def upload_single_file(file_to_upload, conf, cmems_ftp):
    """
    Uploads a single file to FTP connection.
    Retreive status code of the upload and feeds metadata to the FileSync object.
    :param file_to_upload: LocalFileSync instance
    :param conf: (SyncConfiguration) instance
    :param cmems_ftp: FTP instance that is already cwd to the destination dir
    :return: the FileSync instance.
    """
    ftp_conn = cmems_ftp.get_write_ftp_conn()
    if conf.transfer["checkIfFileOnDBS"]:
        try:
            remote_file_datetime = ftp_conn.sendcmd("MDTM {}".format(file_to_upload.relative_path)).split()[1]
        except (ftplib.error_perm, TypeError, ValueError) as e:
            # File is not on the DBS
            remote_file_datetime = 0
        if remote_file_datetime != 0:
            remote_file_datetime = datetime.datetime.strptime(remote_file_datetime, "%Y%m%d%H%M%S")
            time_delta = datetime.datetime.now() - remote_file_datetime
            if time_delta.days == 0:
                conf.cmems_logger.log_info("File {} is already on the DBS since less than 24 hours but not ingested yet. Not uploading this time.".format(file_to_upload.absolute_path))
                file_to_upload.final_status = "Cancelled"
                return file_to_upload
            else:
                conf.cmems_logger.log_info("File {} is already on the DBS since more than 24 hours but not ingested yet. I will overwrite it now.".format(file_to_upload.absolute_path))
    file_to_upload.start_upload_time = strftime("%Y%m%dT%H%M%SZ", gmtime())
    ftp_error_code = ""
    ftp_error_message = ""
    file_to_upload.number_of_attempts += 1

    try:
        with open(file_to_upload.absolute_path, 'rb') as file_bin:
            tmp_md5_hash = hashlib.md5()
            tmp_md5_hash.update(file_bin.read())
            file_to_upload.checksum = tmp_md5_hash.hexdigest()
            file_bin.seek(0)
            try:
                conf.cmems_logger.log_debug("Will upload {} to {}".format(file_to_upload.relative_path, ftp_conn.pwd()))
                ftp_conn.storbinary("STOR {}".format(file_to_upload.relative_path), file_bin)
                file_to_upload.stop_upload_time = strftime("%Y%m%dT%H%M%SZ", gmtime())
                file_to_upload.final_status = "Delivered"
                return file_to_upload
            except (ftplib.error_reply, ftplib.error_temp, ftplib.error_perm, ftplib.error_proto, IOError) as e:
                # If it is a FTP error, we retreive code and message
                if not isinstance(e, IOError):
                    ftp_error_code = str(e).split(" ", 1)[0]
                    ftp_error_message = str(e).split(" ", 1)[1]
                    conf.cmems_logger.log_info("Could not upload file {} due to error : {} - {}".format(file_to_upload.name, ftp_error_code, ftp_error_message))
                # Else we put a 0 code and explicit message :
                else:
                    ftp_error_code = '0'
                    ftp_error_message = "Non FTP-related error has occured while uploading {} : {}".format(file_to_upload.absolute_path, str(e))
                    conf.cmems_logger.log_info("Could not upload file {} due to an error not FTP-related.".format(file_to_upload.name))
                    conf.cmems_logger.log_info(str(e))
                # We register the failed attempt, but if the same error code & message have already occured, we keep the previous one
                # and just change the number of attempts :
                failed_attempt = next((fa for fa in file_to_upload.failed_attempts if (fa["due_to_error_code"] == ftp_error_code and fa["due_to_error_message"] == ftp_error_message)), None)
                if failed_attempt is None:
                    failed_attempt = {}
                    failed_attempt["due_to_error_code"] = ftp_error_code
                    failed_attempt["due_to_error_message"] = ftp_error_message
                    file_to_upload.failed_attempts.append(failed_attempt)
    
                failed_attempt["number_of_attempts"] = copy(file_to_upload.number_of_attempts)
    
                if file_to_upload.number_of_attempts >= conf.transfer["sendingAttempts"]:
                    file_to_upload.final_status = "Failed"
                    conf.cmems_logger.log_warning("Error while uploading {} : {}".format(file_to_upload.absolute_path, str(e)))
                else:
                    conf.cmems_logger.log_info("Retrying the upload.")

                file_to_upload.final_status = "Failed"
    except FileNotFoundError:
        conf.cmems_logger.log_warning("Cancelling upload of {} : file not found.".format(file_to_upload.absolute_path))
        file_to_upload.final_status = "Cancelled"

    finally:
        return file_to_upload


def write_dnt(conf, reference_date, **kwargs):
    """
    Reads all FileSync contained in the object and creates a DNT.
    When the DNT is ready, it is written on local filesytem and
    pushed to the FTP location specified in configuration file.
    :param conf: (SyncConfiguration) instance
    :param reference_date: date to use in DNT
    :param kwargs:
        - files_to_upload
        - files_to_delete
    :return:
    """
    dnt_file_name = "{}_P{}_{}_{}.xml".format(conf.dataset["productId"], reference_date, conf.dataset["datasetId"], conf.dataset["datasetVersion"])
    # ========================================
    #              Root
    # =======================================
    root_node = ET.Element("delivery",
                           product=conf.dataset["productId"],
                           PushingEntity=conf.dataset["pushingEntity"],
                           date=reference_date
                           )
    dataset_node = ET.SubElement(root_node,
                                 "dataset",
                                 DatasetName=f"{conf.dataset['datasetId']}_{conf.dataset['datasetVersion']}"
                                 )
    # ========================================
    #              Uploads
    # =======================================
    if "files_to_upload" in kwargs.keys():
        for f in kwargs.get("files_to_upload", []):
            # We don't add cancelled files in the DNT.
            if not f.final_status == "Cancelled":
                se = ET.SubElement(dataset_node,
                                   "file",
                                   FileName=f.relative_path,
                                   StartUploadTime=f.start_upload_time,
                                   StopUploadTime=f.stop_upload_time,
                                   Checksum=f.checksum,
                                   FinalStatus=f.final_status
                                   )

                for failed_attempt in f.failed_attempts:
                    if failed_attempt["due_to_error_code"] != '0':
                        ET.SubElement(se,
                                      "resendAttempt",
                                      DueToErrorCode=failed_attempt["due_to_error_code"],
                                      DueToErrorMsg=failed_attempt["due_to_error_message"],
                                      NumberOfAttempts=str(failed_attempt["number_of_attempts"])
                                      )
    # ========================================
    #              Deletions
    # =======================================
    if "files_to_delete" in kwargs.keys():
        for f in kwargs.get("files_to_delete", []):
            fd = ET.SubElement(dataset_node,
                               "file",
                               FileName=f.relative_path
                               )
            ET.SubElement(fd, "KeyWord").text = "Delete"

    if "dirs_to_delete" in kwargs.keys():
        for f in kwargs.get("dirs_to_delete", []):
            fd = ET.SubElement(dataset_node,
                               "directory",
                               SourceFolderName=f,
                               DestinationFolderName=""
                               )
            ET.SubElement(fd, "KeyWord").text = "Delete"

    # ========================================
    #              Write DNT
    # =======================================
    raw_string = ET.tostring(root_node, 'utf-8')
    reparsed = minidom.parseString(raw_string)
    tmp_dnt = "{}/{}".format(
                             conf.transfer["tmpDeliveryNotePath"],
                             dnt_file_name
                            )
    with open(tmp_dnt, "w") as f:
        f.write(reparsed.toprettyxml(indent="  "))

    return dnt_file_name


def upload_dnt(conf, dnt_file_name, cmems_ftp):
    """
    Upload DNT to the FTP
    :param conf: (SyncConfiguration) instance
    :param dnt_file_name: dnt file to upload
    :param cmems_ftp: FTP instance that is already cwd to the destination dir
    :return: None
    """
    destdntpath = f"/{conf.dataset['productId']}/DNT"
    ftp_conn = cmems_ftp.get_write_ftp_conn()
    ftp_conn.cwd(destdntpath)
    conf.cmems_logger.log_info("Uploading temporary DNT file.")
    tmp_dnt = "{}/{}".format(
                             conf.transfer["tmpDeliveryNotePath"],
                             dnt_file_name
                            )
    temporary_dnt_filename = "{}.tmp".format(dnt_file_name)
    with open(tmp_dnt, 'rb') as file_bin:
        ftp_conn.storbinary("STOR {}".format(temporary_dnt_filename), file_bin)
    conf.cmems_logger.log_info("Renaming DNT file from {} to {}".format(temporary_dnt_filename, dnt_file_name))
    ftp_conn.rename(temporary_dnt_filename, dnt_file_name)
