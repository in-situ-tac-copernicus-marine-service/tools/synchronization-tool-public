#!/usr/bin/env python
import sys
import logging


def clean_string(input_string):
    input_string = input_string.replace('<', '[')
    input_string = input_string.replace('>', ']')
    input_string = input_string.replace('&quot;', "'")
    input_string = input_string.replace('"', "'")
    return input_string


class CmemsLogger(object):
    """
    Versatile logging class that handles either Coriolis report logging style and default logging.
    """

    __slots__ = 'is_coriolis_report', 'is_default_logging', 'xml_report', 'logger', 'file_loglevel', 'stdout_loglevel'

    def __init__(self, xml_process_report=None, **kwargs):
        """
        If a XmlReport is added to the instance, then we create a Coriolis Report logger that
        will handle TBO. Otherwise, classic python logger will be used.
        """
        if xml_process_report is not None:
            self.is_coriolis_report = True
            self.is_default_logging = False
        else:
            self.is_coriolis_report = False
            self.is_default_logging = True

        self.xml_report = xml_process_report
        file_loglevel = kwargs.get("file_loglevel", None)
        if file_loglevel is not None:
            if file_loglevel == "error":
                self.file_loglevel = logging.ERROR
            elif file_loglevel == "warning":
                self.file_loglevel = logging.WARNING
            elif file_loglevel == "info":
                self.file_loglevel = logging.INFO
            elif file_loglevel == "debug":
                self.file_loglevel = logging.DEBUG
        else:
            self.file_loglevel = 0

        stdout_loglevel = kwargs.get("stdout_loglevel", None)
        if stdout_loglevel is not None:
            if stdout_loglevel == "error":
                self.stdout_loglevel = logging.ERROR
            elif stdout_loglevel == "warning":
                self.stdout_loglevel = logging.WARNING
            elif stdout_loglevel == "info":
                self.stdout_loglevel = logging.INFO
            elif stdout_loglevel == "debug":
                self.stdout_loglevel = logging.DEBUG
        else:
            self.stdout_loglevel = 0

        if self.is_default_logging:
            self.logger = logging.getLogger("mds_insitu_sync")
            # Setting the lowest log level so all other log levels are taken into account
            self.logger.setLevel(logging.DEBUG)

            output = logging.StreamHandler(sys.stdout)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            output.setFormatter(formatter)
            if self.stdout_loglevel is not None:
                output.setLevel(self.stdout_loglevel)
            self.logger.addHandler(output)
        else:
            self.logger = None

    def add_file_handler(self, log_file_path):
        handler = logging.FileHandler(log_file_path)
        handler.setLevel(self.file_loglevel)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def log_error(self, message):
        if self.is_coriolis_report:
            message = clean_string(message)
            self.xml_report.addError(message)
        else:
            self.logger.error(message)

    def log_error_and_exit(self, message):
        if self.is_coriolis_report:
            message = clean_string(message)
            self.xml_report.addError(message)
            self.xml_report.addDuration()
            self.xml_report.addStatusNok()
            self.xml_report.end()
        else:
            self.logger.error(message)
        sys.exit()

    def log_warning(self, message):
        if self.is_coriolis_report:
            message = clean_string(message)
            self.xml_report.addWarning(message)
        else:
            self.logger.warning(message)

    def log_info(self, message):
        if self.is_coriolis_report:
            if self.file_loglevel in [logging.INFO, logging.DEBUG] or self.stdout_loglevel in [logging.INFO, logging.DEBUG]:
                message = clean_string(message)
                self.xml_report.addInfo(message)
        else:
            self.logger.info(message)

    def log_debug(self, message):
        if self.is_coriolis_report:
            if self.file_loglevel == logging.DEBUG or self.stdout_loglevel == logging.DEBUG:
                message = clean_string(message)
                self.xml_report.addInfo(message)
        else:
            self.logger.debug(message)

    def log_end(self):
        if self.is_coriolis_report:
            if "</coriolis_function_report>" not in self.xml_report.get_report():
                self.xml_report.addDuration()
                self.xml_report.addStatusOk()
                self.xml_report.end()

    def report_nb_files_to_upload(self, nb):
        if self.is_coriolis_report:
            self.xml_report.addValue("nb_files_to_upload", nb)

    def report_nb_files_uploaded(self, nb):
        if self.is_coriolis_report:
            self.xml_report.addValue("nb_files_uploaded", nb)

    def report_nb_files_failed(self, nb):
        if self.is_coriolis_report:
            self.xml_report.addValue("nb_files_failed", nb)

    def report_nb_files_to_delete(self, nb):
        if self.is_coriolis_report:
            self.xml_report.addValue("nb_files_to_delete", nb)

    def report_nb_files_cancelled(self, nb):
        if self.is_coriolis_report:
            self.xml_report.addValue("nb_files_cancelled", nb)
