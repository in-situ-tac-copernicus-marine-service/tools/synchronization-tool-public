#!/usr/bin/env python
import ftplib
import os
from time import sleep
import socket


class CmemsFTP(object):
    """
    This class purpose is to give a FTP access to CMEMS-DU FTP servers.
    It uses credentials from a SyncConfiguration object.
    """

    __slots__ = 'sync_configuration', 'write_ftp_access', 'accessible_cwd_path'

    def __init__(self, sync_configuration):
        self.sync_configuration = sync_configuration
        self.write_ftp_access = None
        self.accessible_cwd_path = f"/{self.sync_configuration.dataset['productId']}/{self.sync_configuration.dataset['datasetId']}_{self.sync_configuration.dataset['datasetVersion']}"
        i = 0
        while self.write_ftp_access is None:
            if i >= 3:
                self.sync_configuration.cmems_logger.log_error_and_exit("Retried 3 times to get a DBS connection, but it failed. Aborting job.")
            self.write_ftp_access = self.connect_dbs()
            sleep(1)
            i += 1

        self.write_ftp_access.sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)

    def connect_dbs(self):
        """
        Get access to writable FTP.
        Returns a FTP object which path is already set
        to accessible_cwd_path value filled with "/<ProductId>/<DatasetId>_<DatasetVersion>" from configuration file.
        """
        try:
            f = ftplib.FTP(
                self.sync_configuration.ftp["writeHost"],
                self.sync_configuration.ftp["writeUserName"],
                self.sync_configuration.ftp["writeUserPassword"]
                )
        except ftplib.all_errors as ae:
            self.sync_configuration.cmems_logger.log_error("An error occured trying to contact ftp server : {}. Error details below :".format(self.sync_configuration.ftp["writeHost"]))
            return None
            # self.sync_configuration.cmems_logger.log_error_and_exit(str(ae))

        try:
            f.cwd(self.accessible_cwd_path)
            return f
        except ftplib.error_perm as ep:
            error_msg = "Destination write path {} is not accessible. Please check your configuration file. Exiting.".format(self.accessible_cwd_path)
            self.sync_configuration.cmems_logger.log_error_and_exit(error_msg)

    def get_write_ftp_conn(self, reconnect_on_failure=True):
        """
        Get access to writable FTP.
        Returns a FTP object which path is already set
        to accessible_cwd_path value filled with "/<ProductId>/<DatasetId>_<DatasetVersion>" from configuration file.
        """
        try:
            self.write_ftp_access.cwd(self.accessible_cwd_path)
            return self.write_ftp_access

        except (ftplib.error_temp, EOFError, BrokenPipeError, TimeoutError):
            return self.connect_dbs()

        except ftplib.error_perm as ep:
            error_msg = "Destination write path {} is not accessible. Please check your configuration file. Exiting.".format(self.accessible_cwd_path)
            self.sync_configuration.cmems_logger.log_error_and_exit(error_msg)
