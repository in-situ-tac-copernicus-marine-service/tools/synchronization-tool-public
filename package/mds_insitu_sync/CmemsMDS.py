#!/usr/bin/env python
import os
from pathlib import Path
import boto3
from botocore import UNSIGNED
from botocore.client import Config
from urllib.request import urlopen
from urllib.parse import urlsplit
from packaging import version
import json


class CmemsMDS(object):
    """
    This class purpose is to implement an MDS objet. It also define the cache folder in environment variables.
    """
    __slots__ = 'sync_configuration', 'copernicusmarine'

    def __init__(self, sync_configuration):
        self.sync_configuration = sync_configuration
        os.environ["COPERNICUSMARINE_CACHE_DIRECTORY"] = self.sync_configuration.mds["cacheDirectory"]
        self.copernicusmarine = None

    @staticmethod
    def split_copernicus_url(url):
        split_url = urlsplit(url)
        endpoint = split_url.scheme + "://" + split_url.netloc
        bucket = split_url.path.split("/")[1]
        path = "/".join(split_url.path.split("/")[2:])
        if not path.endswith("/"):
            path += "/"

        return endpoint, bucket, path

    @staticmethod
    def request_files_list(bucket, prefix, endpoint=None):
        # Initialize the S3 client without authentication
        s3 = boto3.client('s3', config=Config(signature_version=UNSIGNED), endpoint_url=endpoint)

        # Response without paginator
        # response = s3.list_objects_v2(Bucket=bucket, Prefix=prefix,Delimiter="/")

        paginator = s3.get_paginator('list_objects_v2')
        pages = paginator.paginate(Bucket=bucket, Prefix=prefix)

        files = []
        for page in pages:
            for obj in page['Contents']:
                # print(f"{obj['Key']}: {obj['LastModified']} - {obj['Size'] / 1000}")
                files.append(f"s3://{bucket}/{obj['Key']}")
        return files

    def list_remote_files_with_copernicusmarine_api(self, output_file):
        import copernicusmarine
        self.copernicusmarine = copernicusmarine
        current_version = self.copernicusmarine.__version__

        # get common params
        get_params = {
            "dataset_id": self.sync_configuration.dataset["datasetId"],
            "dataset_version": self.sync_configuration.dataset["datasetVersion"],
            "dataset_part": self.sync_configuration.dataset["datasetPart"],
            "username": self.sync_configuration.mds["readUserName"],
            "password": self.sync_configuration.mds["readUserPassword"],
            "create_file_list": output_file,
            "output_directory": Path(self.sync_configuration.transfer["tmpCopyPath"]),
        }

        # Add "service" param for cmt versions below 2.0
        if version.parse(current_version) < version.parse("2.0.0a0"):
            get_params["service"] = "original-files"

        self.copernicusmarine.get(**get_params)

    def list_remote_files_with_custom_method(self, output_file):
        product_name = self.sync_configuration.dataset["productId"]
        dataset_name = self.sync_configuration.dataset["datasetId"] + "_" + self.sync_configuration.dataset["datasetVersion"]
        if self.sync_configuration.dataset["datasetPart"]:
            dataset_name = dataset_name + "--ext--" + self.sync_configuration.dataset["datasetPart"]

        metadata_url = f"https://stac.marine.copernicus.eu/metadata/{product_name}/{dataset_name}/dataset.stac.json"

        # Retrieve S3 files URL of the product/dataset
        with urlopen(metadata_url) as response:
            data = json.load(response)

        direct_files_url = ''
        for asset_id, asset_info in data['assets'].items():
            if asset_info["id"] == "native":
                direct_files_url = asset_info['href']
                break

        endpoint_url, bucket_name, prefix_path = self.split_copernicus_url(direct_files_url)
        file_paths = self.request_files_list(bucket_name, prefix_path, endpoint_url)

        # Écrire les chemins dans un fichier .txt
        with open(Path(self.sync_configuration.transfer["tmpCopyPath"]) / output_file, 'w') as f:
            for file in file_paths:
                f.write(file + '\n')
