#!/usr/bin/env python
import os
import configparser
from time import strftime
from datetime import datetime


class SyncConfiguration(object):
    """
    This class represents the configuration file given by the operator.
    """
    __slots__ = 'mds', 'ftp', 'dataset', 'transfer', 'expected_config_sections', 'missing_sections', 'cmems_logger', 'rollback_file_path'

    def __init__(self, cmems_logger):
        # Adapt below dictionnary if you want to add more configuration items to MDS section
        self.mds = {
            "readUserName": "",
            "readUserPassword": "",
            "cacheDirectory": "",
            "remoteListingMethod": ""
        }
        # Adapt below dictionnary if you want to add more configuration items to FTP section
        self.ftp = {
            "writeHost": "",
            "writeUserName": "",
            "writeUserPassword": ""
        }
        # Adapt below dictionnary if you want to add more configuration items to dataset section
        self.dataset = {
            "pushingEntity": "",
            "productId": "",
            "datasetId": "",
            "datasetVersion": "",
            "datasetPart": "",
            "timestampFile": "",
            "sourceDatasetPath": "",
            "sourceIndexDataFile": "",
            "sourceIndexPlatformFile": "",
            "dateUpdateFromNetcdf": False,
            "excludeRegex": "",
            "listingMethod": ""
        }
        # Adapt below dictionnary if you want to add more configuration items to transfer section
        self.transfer = {
            "tmpDeliveryNotePath": "",
            "tmpCopyPath": "",
            "transferLogFilePath": "",
            "parallelTransfer": False,
            "parallelTransferCount": 5,
            "sendingAttempts": 1,
            "timeout": None,
            "splitTransfer": 0,
            "checkIfFileOnDBS": False
        }
        # Customize below attribute if you want to add mandatory sections in configuration file.
        self.expected_config_sections = {'MDS', 'FTP', 'Dataset', 'Transfer'}
        self.missing_sections = []
        self.cmems_logger = cmems_logger
        self.rollback_file_path = ""

    def has_expected_sections(self, file_path):
        """
        Uses expected_config_sections attribute and compares it to the sections given in
        the configuration file. If the expected sections are not all mentioned in the configuration
        file, then the program will stop.
        """
        if not os.path.exists(file_path):
            error_msg = "Provided file configuration path {} does not exist.".format(file_path)
            self.cmems_logger.log_error_and_exit(error_msg)
        parser = configparser.ConfigParser()
        try:
            parser.read(file_path)
        except configparser.MissingSectionHeaderError:
            error_msg = "Configuration file {} is invalid. Please have a look at configuration notice.".format(file_path)
            self.cmems_logger.log_error_and_exit(error_msg)
        actual_config_sections = parser.sections()
        self.missing_sections = list(self.expected_config_sections - set(actual_config_sections))
        if len(self.missing_sections) == 0:
            return True
        else:
            return False

    def set_configuration(self, configuration_file_path):
        """
        Reads the configuration file and sets attributes according values. 
        Handles mandatory and optional configuration items.
        """
        if not self.has_expected_sections(configuration_file_path):
            error_msg = "Check your configuration file. Following sections are missing : {}.".format(self.missing_sections)
            self.cmems_logger.log_error_and_exit(error_msg)
        parser = configparser.ConfigParser()
        parser.read(configuration_file_path)
        # Mandatory configuration items :
        try:
            self.mds["readUserName"] = parser.get('MDS', 'ReadUserName')
            self.mds["readUserPassword"] = parser.get('MDS', 'ReadUserPassword')
            self.mds["cacheDirectory"] = parser.get('MDS', 'CacheDirectory')
            self.mds["remoteListingMethod"] = parser.get('MDS', 'RemoteListingMethod')
            if self.mds["remoteListingMethod"] not in ["copernicusmarine_api", "direct_s3", "disabled"]:
                self.cmems_logger.log_error_and_exit(
                    "{} is not a correct value for RemoteListingMethod configuration item. Expected values: ['copernicusmarine_api', 'direct_s3', 'disabled']"
                    .format(self.mds["remoteListingMethod"]))

            self.ftp["writeHost"] = parser.get('FTP', 'WriteHost')
            self.ftp["writeUserName"] = parser.get('FTP', 'WriteUserName')
            self.ftp["writeUserPassword"] = parser.get('FTP', 'WriteUserPassword')

            self.dataset["pushingEntity"] = parser.get('Dataset', 'PushingEntity')
            self.dataset["productId"] = parser.get('Dataset', 'ProductId')
            self.dataset["datasetId"] = parser.get('Dataset', 'DatasetId')
            self.dataset["datasetVersion"] = parser.get('Dataset', 'DatasetVersion')

            self.dataset["sourceDatasetPath"] = parser.get('Dataset', 'SourceDatasetPath')
            self.dataset["listingMethod"] = parser.get('Dataset', 'ListingMethod')
            if not self.dataset["listingMethod"] in ["file"]:
                self.cmems_logger.log_error_and_exit(
                    "{} is not a correct value for ListingMethod configuration item. Expected values: ['file']".format(
                        self.dataset["listingMethod"]))

            # TODO waiting index files management
            """
            if self.dataset["listingMethod"] == "index" and not parser.get('Dataset', 'SourceIndexDataFile'):
                self.cmems_logger.log_error_and_exit(
                    "You set ListingMethod to 'index' but no index file was specified for SourceIndexDataFile item.")
            """

            self.transfer["tmpDeliveryNotePath"] = parser.get('Transfer', 'TmpDeliveryNotePath')
            self.transfer["tmpCopyPath"] = parser.get('Transfer', 'TmpCopyPath')
        except configparser.Error as e:
            self.cmems_logger.log_error_and_exit(str(e))

        # Optional items :
        try:
            self.dataset["datasetPart"] = parser.get('Dataset', 'DatasetPart')
        except configparser.NoOptionError:
            pass

        try:
            self.dataset["timestampFile"] = parser.get('Dataset', 'TimestampFile')
        except configparser.NoOptionError as noe:
            if self.dataset["listingMethod"] == "index":
                pass
            else:
                error_msg = "When setting ListingMethod to file, TimestampFile value is mandatory."
                self.cmems_logger.log_error(error_msg)
                self.cmems_logger.log_error_and_exit(str(noe))

        try:
            self.dataset["sourceIndexPlatformFile"] = parser.get('Dataset', 'SourceIndexPlatformFile')
        except configparser.NoOptionError:
            pass

        try:
            self.dataset["sourceIndexDataFile"] = parser.get('Dataset', 'SourceIndexDataFile')
        except configparser.NoOptionError:
            pass

        try:
            if parser.get('Dataset', 'DateUpdateFromNetcdf') == 'true':
                self.dataset["dateUpdateFromNetcdf"] = True
        except configparser.NoOptionError:
            pass

        try:
            self.dataset["excludeRegex"] = parser.get('Dataset', 'ExcludeRegex')
        except configparser.NoOptionError:
            pass

        try:
            # If a logfile path is defined in conf. Add a new filehandler to logging instance.
            if parser.get('Transfer', 'TransferLogFilePath') != "":
                log_file = "{}/{}-{}-{}_{}.log".format(
                    parser.get('Transfer', 'TransferLogFilePath'),
                    strftime("%Y%m%dT%H%M%SZ", datetime.now().timetuple()),
                    self.dataset["productId"],
                    self.dataset["datasetId"],
                    self.dataset["datasetVersion"]
                    )
                self.cmems_logger.add_file_handler(log_file)
                self.transfer["transferLogFilePath"] = parser.get('Transfer', 'TransferLogFilePath')
        except configparser.NoOptionError:
            pass

        try:
            if parser.get('Transfer', 'ParallelTransfer') == 'true':
                self.transfer["parallelTransfer"] = True
        except configparser.NoOptionError:
            pass
        try:
            paralleltransfercount = parser.getint('Transfer', 'ParallelTransferCount')
            if paralleltransfercount > 5:
                self.transfer["parallelTransferCount"] = 5
            else:
                self.transfer["parallelTransferCount"] = paralleltransfercount
        except configparser.NoOptionError:
            pass
        try:
            self.transfer["sendingAttempts"] = parser.getint('Transfer', 'SendingAttempts')
        except configparser.NoOptionError:
            pass
        try:
            self.transfer["timeout"] = parser.getint('Transfer', 'Timeout') * 60
        except configparser.NoOptionError:
            pass
        try:
            self.transfer["splitTransfer"] = parser.getint('Transfer', 'SplitTransfer')
        except configparser.NoOptionError:
            pass
        try:
            if parser.get('Transfer', 'CheckIfFileOnDBS') == 'true':
                self.transfer["checkIfFileOnDBS"] = True
        except configparser.NoOptionError:
            pass
