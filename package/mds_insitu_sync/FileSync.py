#!/usr/bin/env python

from collections import defaultdict
import os
import hashlib
import time
import datetime
from netCDF4 import Dataset


class FileSync(object):
    """
    This class represents a file with path attributes.
    """
    __slots__ = 'name', 'relative_path', 'absolute_path'

    def __init__(self, name, relative_path, absolute_path):
        self.name = name
        self.relative_path = relative_path
        self.absolute_path = absolute_path


class LocalFileSync(FileSync):
    """
    This class represents a file on local filesystem.
    It has attributes that will handle a FTP action and the DNT writings operations.
    One can find a specific LocalFileSync object through an array by its relative path.
    """

    __slots__ = 'mtime', 'to_be_uploaded', 'checksum', 'start_upload_time', 'stop_upload_time', 'final_status', \
                'number_of_attempts', 'failed_attempts', 'last_modification_timestamp'
    path_index = defaultdict(list)

    def __init__(self, name, relative_path, absolute_path, **kwargs):
        FileSync.__init__(self, name, relative_path, absolute_path)
        self.mtime = os.stat(self.absolute_path).st_mtime
        self.to_be_uploaded = False
        self.checksum = ""
        self.start_upload_time = ""
        self.stop_upload_time = ""
        self.final_status = ""
        self.number_of_attempts = 0
        self.failed_attempts = []
        self.last_modification_timestamp = 0

        if "date_update_from_netcdf" in kwargs.keys():
            f = Dataset(self.absolute_path, "r")
            self.last_modification_timestamp = int(time.mktime(datetime.datetime.strptime(f.date_update, "%Y-%m-%dT%H:%M:%SZ").timetuple()))
        LocalFileSync.path_index[self.relative_path].append(self)

    @classmethod
    def find_by_relative_path(cls, path):
        return LocalFileSync.path_index[path]

    def get_checksum(self):
        tmp_md5_hash = hashlib.md5()
        with open(self.absolute_path, 'rb') as f:
            for chunk in iter(lambda: f.read(65536), b""):
                tmp_md5_hash.update(chunk)
        return tmp_md5_hash.hexdigest()

    def set_checksum(self):
        tmp_md5_hash = hashlib.md5()
        with open(self.absolute_path, 'rb') as f:
            for chunk in iter(lambda: f.read(65536), b""):
                tmp_md5_hash.update(chunk)
        self.checksum = tmp_md5_hash.hexdigest()


class RemoteFileSync(FileSync):
    """
    This class represents a file on remote FTP.
    It has attributes that will handle a FTP deletions.
    One can find a specific RemoteFileSync object through an array by its relative path.
    """

    path_index = defaultdict(list)

    def __init__(self, name, relative_path, absolute_path):
        FileSync.__init__(self, name, relative_path, absolute_path)
        self.to_be_deleted = False
        RemoteFileSync.path_index[self.relative_path].append(self)

    @classmethod
    def find_by_relative_path(cls, path):
        return RemoteFileSync.path_index[path]
