#!/usr/bin/env python
import sys
import re
import os
import signal
import linecache
import datetime
import tempfile
import shutil
from time import time, strftime, gmtime, sleep, mktime
from pathlib import Path
from mds_insitu_sync.FileSync import LocalFileSync, RemoteFileSync
import mds_insitu_sync.SyncOperations as sync_ops
from mds_insitu_sync.SyncConfiguration import SyncConfiguration
from mds_insitu_sync.CmemsLogging import CmemsLogger
from mds_insitu_sync.CmemsFTP import CmemsFTP
from mds_insitu_sync.CmemsMDS import CmemsMDS
import ftplib
import psutil


class SyncJob(object):
    """
    This class represents a synchronization job. A SyncJob instance is characterized by :
        - a SyncOperations instance that describes the sync operations to process
        - a SyncConfiguration instance that specifies the way to process sync operations
    """

    __slots__ = 'sync_conf', 'dry_mode', 'last_job_timestamp', 'current_job_timestamp', 'job_start_time', \
                'local_files', 'remote_files', 'nb_files_to_upload', 'nb_files_to_delete', \
                'nb_files_uploaded', 'nb_files_failed', 'nb_files_cancelled', 'files_to_upload', 'files_to_delete', \
                'index_data_file_copy', 'index_platform_file_copy', 'cmems_ftp', 'cmems_mds'

    def __init__(self, configuration_file_path, cmems_logger):

        self.sync_conf = SyncConfiguration(cmems_logger)
        self.sync_conf.set_configuration(configuration_file_path)

        try:
            re.compile(self.sync_conf.dataset["excludeRegex"])
        except re.error:
            self.sync_conf.cmems_logger.log_error_and_exit("The provided ExcludeRegex value is not a valid regular expression. "
                                                           "Please check it and try again. ")

        self.dry_mode = False

        if self.sync_conf.dataset["listingMethod"] == "file":
            self.last_job_timestamp = sync_ops.read_timestamp_file(self.sync_conf)
        else:
            self.last_job_timestamp = 0

        self.current_job_timestamp = int(time())
        self.job_start_time = strftime("%Y%m%dT%H%M%SZ", gmtime())

        self.local_files = []
        self.remote_files = []

        self.files_to_upload = []
        self.files_to_delete = []

        self.nb_files_to_upload = 0
        self.nb_files_to_delete = 0
        self.nb_files_uploaded = 0
        self.nb_files_failed = 0
        self.nb_files_cancelled = 0

        self.cmems_ftp = CmemsFTP(self.sync_conf)
        self.cmems_mds = CmemsMDS(self.sync_conf)

        # Create a copy of the index file & point to this copy during the sync job :
        # We work with a copy because some sync jobs can be long & we don't want to upload 
        # a version of the index file which will be more recent than the list of the files that we are about to upload
        if self.sync_conf.dataset["sourceIndexDataFile"] != "":
            self.index_data_file_copy = self.copy_index_file(self.sync_conf.dataset["sourceIndexDataFile"])
        # Same for index_platform
        if self.sync_conf.dataset["sourceIndexPlatformFile"] != "":
            self.index_platform_file_copy = self.copy_index_file(self.sync_conf.dataset["sourceIndexPlatformFile"])

    def i_have_been_killed(self, signal, frame):
        self.sync_conf.cmems_logger.log_error("Process has been killed. Either timeout was triggered or the process was terminated from outside.")
        self.rollback()
        me = psutil.Process(os.getpid())
        my_children = me.children(recursive=True)
        for child in my_children:
            child.send_signal(signal)
        sys.exit(0)

    def rollback(self):
        # Creating a lock file in the bin directory.
        self.sync_conf.cmems_logger.log_warning("Something went wrong : "
                                                "will create a rollback "
                                                "file for next job : {}".format(self.sync_conf.rollback_file_path))
        Path(self.sync_conf.rollback_file_path).touch()

    def run_sync(self):
        try:
            if self.sync_conf.transfer["timeout"] is not None:
                signal.signal(signal.SIGTERM, self.i_have_been_killed)
            # Check if there is an existing rollback file
            # In this case, we will disable CheckIfFileOnDBS conf value in order to send everything on DBS
            if os.path.exists(self.sync_conf.rollback_file_path):
                self.sync_conf.cmems_logger.log_info("Previous sync job "
                                                     "has been interrupted (found a rollback file). "
                                                     "Will disable checkIfFileOnDBS configuration option.")
                self.sync_conf.transfer["checkIfFileOnDBS"] = False
                os.remove(self.sync_conf.rollback_file_path)

            self.list_local_and_remote_files()

            if self.sync_conf.dataset["listingMethod"] == "file":
                self.find_updated_local_files()
                self.find_deleted_local_files()
            else:
                pass
                # TODO waiting index files management
                # self.diff_local_remote()

            self.sync_files()
            if self.sync_conf.dataset["timestampFile"] != "":
                self.update_timestamp_file()
        finally:
            self.clean()
            self.sync_conf.cmems_logger.log_end()

    def sync_only_missing_files(self):
        try:
            self.list_local_and_remote_files()
            self.find_missing_local_files()
            self.sync_files()
        finally:
            self.clean()
            self.sync_conf.cmems_logger.log_end()

    def list_local_and_remote_files(self):
        # ===========================================
        #
        #         List local & remote files
        #
        # ===========================================
        self.sync_conf.cmems_logger.log_info("List local files")
        self.local_files = sync_ops.list_local_files(self.sync_conf)

        # If no local files found at all, it's not normal
        if len(self.local_files) == 0:
            self.sync_conf.cmems_logger.log_error_and_exit("No local files founds. Please check SourceDatasetPath and try again.")

        if self.sync_conf.mds["remoteListingMethod"] == 'disabled':
            self.sync_conf.cmems_logger.log_info("remoteListingMethod option is disabled. Won't list remote files.")
        else:
            self.sync_conf.cmems_logger.log_info("List remote files")
            self.remote_files = sync_ops.list_remote_files(self.sync_conf, self.cmems_mds, self.current_job_timestamp)
            # TODO Should we use this ? Could be boring on every first run
            # if len(self.remote_files) == 0:
            #    self.sync_conf.cmems_logger.log_error_and_exit("No remote files founds. If it's a normal situation, please set "
            #                                                   "remoteListingMethod = 'disabled' option. If not, please check Dataset configurations and try again.")

    def find_missing_local_files(self):
        for missing in set([lf.relative_path for lf in self.local_files]) - \
                       set([rf.relative_path for rf in self.remote_files]):
            LocalFileSync.find_by_relative_path(missing)[0].to_be_uploaded = True
        self.files_to_upload = [lf for lf in self.local_files if lf.to_be_uploaded]

    def diff_local_remote(self):
        # TODO: Not Used
        self.sync_conf.cmems_logger.log_debug("Started diff between local files and remote files.")
        self.sync_conf.cmems_logger.log_debug("Finding local files to upload.")
        for f in self.local_files:
            tmp_rfs = RemoteFileSync.find_by_relative_path(f.relative_path)

            if len(tmp_rfs) > 0:
                if f.last_modification_timestamp > tmp_rfs[0].last_modification_timestamp:
                    f.to_be_uploaded = True
                    self.files_to_upload.append(f)
                    self.nb_files_to_upload += 1
                else:
                    f.to_be_uploaded = False
            else:
                f.to_be_uploaded = True
                self.files_to_upload.append(f)
                self.nb_files_to_upload += 1
        self.sync_conf.cmems_logger.log_info("Number of files to upload : {}".format(self.nb_files_to_upload))

        self.sync_conf.cmems_logger.log_debug("Finding remote files to delete.")
        for f in self.remote_files:
            if len(LocalFileSync.find_by_relative_path(f.relative_path)) < 1:
                f.to_be_deleted = True
                self.files_to_delete.append(f)
                self.nb_files_to_delete += 1
        self.sync_conf.cmems_logger.log_info("Number of files to delete : {}".format(self.nb_files_to_delete))

    def find_updated_local_files(self):
        # ===========================================
        #
        #            Find files to upload
        #
        # ===========================================
        for f in self.local_files:
            # TODO : Should we use this ? Dangerous if remote listing fails
            if self.sync_conf.mds["remoteListingMethod"] != 'disabled':
                # If file doesn't exist in remote server and if remoteListingMethod is activated : push it anyway
                tmp_rfs = RemoteFileSync.find_by_relative_path(f.relative_path)
                if len(tmp_rfs) == 0:
                    f.to_be_uploaded = True
                    continue

            # Compare with netCDF file 'date_update' global attribute if available :
            if f.last_modification_timestamp > 0:
                if f.last_modification_timestamp > self.last_job_timestamp:
                    f.to_be_uploaded = True
            # Or compare unix file attributes :
            else:
                if int(os.stat(f.absolute_path).st_mtime) > self.last_job_timestamp:
                    f.to_be_uploaded = True

        self.files_to_upload = [f for f in self.local_files if f.to_be_uploaded]
        self.nb_files_to_upload = len(self.files_to_upload)
        self.sync_conf.cmems_logger.log_info("Number of files to upload : {}".format(self.nb_files_to_upload))

    def find_deleted_local_files(self):
        # ===========================================
        #
        #            Find files to delete
        #
        # ===========================================
        for df in set([rf.relative_path for rf in self.remote_files]) - \
                  set([lf.relative_path for lf in self.local_files]):
            RemoteFileSync.find_by_relative_path(df)[0].to_be_deleted = True

        self.files_to_delete = [rf for rf in self.remote_files if rf.to_be_deleted]
        self.nb_files_to_delete = len(self.files_to_delete)
        self.sync_conf.cmems_logger.log_info("Number of files to delete : {}".format(self.nb_files_to_delete))

    def copy_index_file(self, file):
        # Create a temporary file & copy the current index file content inside
        new_index_file = tempfile.NamedTemporaryFile(dir=self.sync_conf.transfer["tmpCopyPath"], delete=False)
        shutil.copyfile(file, new_index_file.name)
        
        # Return the temp file absolute path so that it can replace the proper item during the sync job :
        return new_index_file.name

    def sync_files(self):
        # ===========================================
        #
        #                 Sync
        #
        # ===========================================
        if not self.dry_mode:
            self.sync_conf.cmems_logger.log_info("Create needed remote directories.")
            local_subdirs = sync_ops.list_subdirs_from_files(self.files_to_upload)
            sync_ops.create_remote_directories(self.sync_conf, local_subdirs, self.cmems_ftp)
        # ****************************** 
        #              Splitted transfer
        # ******************************
        if self.sync_conf.transfer["splitTransfer"] > 0:
            splitted_files_to_upload = [self.files_to_upload[x:x+self.sync_conf.transfer["splitTransfer"]]
                                        for x in range(0,
                                                       len(self.files_to_upload),
                                                       self.sync_conf.transfer["splitTransfer"])]

            num_chunks = len(splitted_files_to_upload)
            chunk_cursor = 1
            self.sync_conf.cmems_logger.log_info("SPLIT TRANSFER. Number of chunks : {}".format(num_chunks))
            self.files_to_upload = []

            for sublist_files_to_upload in splitted_files_to_upload:
                dnt_date = strftime("%Y%m%dT%H%M%SZ", gmtime())
                if not self.dry_mode:
                    self.sync_conf.cmems_logger.log_info("Uploading chunk {}/{}".format(chunk_cursor, num_chunks))
                    sublist_files_to_upload = sync_ops.upload_files_list(self.sync_conf, sublist_files_to_upload, self.cmems_ftp)
                    chunk_cursor += 1
                else:
                    for f in sublist_files_to_upload:
                        try:
                            f.set_checksum()
                            f.start_upload_time = dnt_date
                            f.stop_upload_time = dnt_date
                            f.final_status = "Delivered"
                        except FileNotFoundError:
                            f.final_status = "Cancelled"
                self.files_to_upload.extend(sublist_files_to_upload)
                if len(sublist_files_to_upload) > 0:
                    self.sync_conf.cmems_logger.log_info("Writing DNT file : {}".format(dnt_date))
                    dnt_file_name = sync_ops.write_dnt(self.sync_conf,
                                                       dnt_date,
                                                       files_to_upload=sublist_files_to_upload)
                    if self.dry_mode:
                        sleep(1)
                    if not self.dry_mode:
                        self.sync_conf.cmems_logger.log_info("Uploading DNT file : {}".format(dnt_file_name))
                        sync_ops.upload_dnt(self.sync_conf, dnt_file_name, self.cmems_ftp)
        # ****************************** 
        #                 Whole transfer
        # ******************************
        else:
            dnt_date = strftime("%Y%m%dT%H%M%SZ", gmtime())
            if not self.dry_mode:
                self.sync_conf.cmems_logger.log_info("WHOLE TRANSFER : Starting upload")
                self.files_to_upload = sync_ops.upload_files_list(self.sync_conf, self.files_to_upload, self.cmems_ftp)
            else:
                for f in self.files_to_upload:
                    try:
                        f.set_checksum()
                        f.start_upload_time = dnt_date
                        f.stop_upload_time = dnt_date
                        f.final_status = "Delivered"
                    except FileNotFoundError:
                        f.final_status = "Cancelled"

            self.sync_conf.cmems_logger.log_info("Writing DNT file : {}".format(dnt_date))
            dnt_file_name = sync_ops.write_dnt(self.sync_conf, dnt_date, files_to_upload=self.files_to_upload)
            if self.dry_mode:
                sleep(1)
            if not self.dry_mode and len(self.files_to_upload) > 0:
                self.sync_conf.cmems_logger.log_info("Uploading DNT file : {}".format(dnt_file_name))
                sync_ops.upload_dnt(self.sync_conf, dnt_file_name, self.cmems_ftp)
        # ****************************** 
        #                     Index files
        # ******************************
        if self.sync_conf.dataset["sourceIndexDataFile"] != "":
            self.sync_index_management(self.sync_conf.dataset["sourceIndexDataFile"], self.index_data_file_copy)
        if self.sync_conf.dataset["sourceIndexPlatformFile"] != "":
            self.sync_index_management(self.sync_conf.dataset["sourceIndexPlatformFile"], self.index_platform_file_copy)

        # ****************************** 
        #                files to delete
        # ******************************
        sleep(1)
        if len(self.files_to_delete) > 0:
            dnt_date = strftime("%Y%m%dT%H%M%SZ", gmtime())
            self.sync_conf.cmems_logger.log_info("Writing DNT file for files deletions: {}".format(dnt_date))
            dnt_file_name = sync_ops.write_dnt(self.sync_conf, dnt_date, files_to_delete=self.files_to_delete)
            if self.dry_mode:
                sleep(1)
            if not self.dry_mode:
                self.sync_conf.cmems_logger.log_info("Uploading DNT file for files deletions : {}".format(dnt_file_name))
                sync_ops.upload_dnt(self.sync_conf, dnt_file_name, self.cmems_ftp)

        # If files failed uploading, we touch them so they will be automatically uploaded next sync job :
        for f in self.files_to_upload:
            if f.final_status == "Failed":
                Path(f.absolute_path).touch(exist_ok=True)

        # Finally close the FTP connections :
        try:
            self.cmems_ftp.get_write_ftp_conn().close()
        except:
            pass

        # ===========================================
        #
        #                Sync stats
        #
        # ===========================================
        self.nb_files_uploaded = len([f for f in self.files_to_upload if f.final_status == "Delivered"])
        self.nb_files_failed = len([f for f in self.files_to_upload if f.final_status == "Failed"])
        self.nb_files_cancelled = len([f for f in self.files_to_upload if f.final_status == "Cancelled"])

        self.sync_conf.cmems_logger.log_info("Job ended")
        self.sync_conf.cmems_logger.report_nb_files_to_upload(self.nb_files_to_upload)
        self.sync_conf.cmems_logger.report_nb_files_to_delete(self.nb_files_to_delete)
        self.sync_conf.cmems_logger.report_nb_files_uploaded(self.nb_files_uploaded)
        self.sync_conf.cmems_logger.report_nb_files_failed(self.nb_files_failed)
        self.sync_conf.cmems_logger.report_nb_files_cancelled(self.nb_files_cancelled)

    def sync_index_management(self, index_file_raw, index_file_copy):
        # Even though we mention sometimes the index file as described in the conf file,
        # the one we want to read is the one copied at the beginning of the job in order
        # to have an index file as "old" as the list of files created sooner in the program
        # Don't upload index_file if already pushed previously

        # Check if the index file can be opened/decoded
        try:
            with open(index_file_copy, 'r') as file:
                file.read()
        except UnicodeDecodeError as e:
            self.sync_conf.cmems_logger.log_error(f"{Path(index_file_raw).name}: Unable to decode the file. It will not be pushed. Error: {str(e)}")
            return None
        except Exception as e:
            self.sync_conf.cmems_logger.log_error(f"{Path(index_file_raw).name}: An error occurred: {str(e)}. It will not be pushed")
            return None

        index_file_update_time_str = linecache.getline(index_file_copy, 5).split(":", 1)[1].strip()
        index_file_update_timestamp = mktime(datetime.datetime.strptime(index_file_update_time_str, "%Y-%m-%dT%H:%M:%SZ").timetuple())

        # Don't upload index_file if already pushed previously
        index_file = next((oif for oif in self.files_to_upload if oif.absolute_path == index_file_raw), None)

        if index_file_update_timestamp > self.last_job_timestamp \
                or self.nb_files_to_upload > 0:

            if index_file:
                self.sync_conf.cmems_logger.log_info(f"Index file {index_file.relative_path} already taken into account")
            else:
                index_file = LocalFileSync(Path(index_file_raw).name, Path(index_file_raw).name, index_file_copy)
                index_file.to_be_uploaded = True
                dnt_date = strftime("%Y%m%dT%H%M%SZ", gmtime())
                if self.dry_mode:
                    try:
                        index_file.set_checksum()
                        index_file.start_upload_time = dnt_date
                        index_file.stop_upload_time = dnt_date
                        index_file.final_status = "Delivered"
                    except FileNotFoundError:
                        index_file.final_status = "Cancelled"
                else:
                    self.sync_conf.cmems_logger.log_info(f"Uploading {index_file.relative_path} file.")
                    sync_ops.upload_single_file(index_file, self.sync_conf, self.cmems_ftp)

                self.sync_conf.cmems_logger.log_info(f"Writing DNT file for {index_file.relative_path} : {dnt_date}")
                dnt_file_name = sync_ops.write_dnt(self.sync_conf, dnt_date, files_to_upload=[index_file])
                if self.dry_mode:
                    sleep(1)
                if not self.dry_mode:
                    if index_file.final_status == "Delivered":
                        self.sync_conf.cmems_logger.log_info(f"Uploading DNT file for {index_file.relative_path} : {dnt_file_name}")
                        sync_ops.upload_dnt(self.sync_conf, dnt_file_name, self.cmems_ftp)
        sleep(1)

    def update_timestamp_file(self):
        if not self.dry_mode:
            self.sync_conf.cmems_logger.log_info("Replacing last job timestamp {} with {}.".format(self.last_job_timestamp, self.current_job_timestamp))
            sync_ops.write_timestamp_file(self.sync_conf, self.current_job_timestamp)

    def clean(self):
        # ===========================================
        #
        #        Clean DNT directory  (old dnt files)
        #
        # ===========================================
        now = time()
        # Clean DNT files
        for f in os.listdir(self.sync_conf.transfer["tmpDeliveryNotePath"]):
            f_path = os.path.join(self.sync_conf.transfer["tmpDeliveryNotePath"], f)
            f_ctime = os.stat(f_path).st_ctime
            if (now - f_ctime) // (24 * 3600) >= 7:
                os.unlink(f_path)

        # Remove temporary index file :
        if self.sync_conf.dataset["sourceIndexDataFile"] != "":
            if os.path.exists(self.index_data_file_copy):
                os.remove(self.index_data_file_copy)

        # Remove temporary index file :
        if self.sync_conf.dataset["sourceIndexPlatformFile"] != "":
            if os.path.exists(self.index_platform_file_copy):
                os.remove(self.index_platform_file_copy)

        # Remove temporary index files that may not have been deleteted properly before
        for tmp_file in Path(self.sync_conf.transfer["tmpCopyPath"]).glob("tmp*"):
            f_ctime = os.stat(tmp_file).st_ctime
            if (now - f_ctime) // (24 * 3600) >= 7:
                os.unlink(tmp_file)
