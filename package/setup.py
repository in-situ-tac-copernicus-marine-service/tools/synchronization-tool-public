#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

setup(
    name='mds_insitu_sync',
    version='3.2.0',
    description='CopernicusMarineInsitu_sync',
    author='Corentin Guyot',
    author_email='',
    packages=find_packages(),
    install_requires=[
        'pathos==0.3.3',
        'netCDF4==1.7.2 ',
        'psutil==6.1.0',
        'packaging==24.2',
        'boto3==1.35.60',
        'botocore==1.35.60',
        'copernicusmarine==1.3.4'
    ]
)