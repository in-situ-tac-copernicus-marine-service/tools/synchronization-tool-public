#!/usr/bin/env python

import sys, os, argparse, inspect
from mds_insitu_sync.CmemsLogging import CmemsLogger
from mds_insitu_sync.SyncJob import SyncJob
from multiprocessing import Process
from common_bigdata.report import XmlReport as xmlProcessReport


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Synchronize your data based on a configuration file path.')
    parser.add_argument('configuration_file', type=str, help='Absolute path to your configuration file.')
    parser.add_argument("--dryrun", help="Only print what actions "
                                         "would be done on remote FTP without executing it.", action="store_true")
    parser.add_argument("--coreport", help="Use coriolis-report as logging "
                                           "method. Log level is set with --stdout-loglevel", action="store_true")
    parser.add_argument("--file-loglevel",type=str, help="Specify the file log level : error | warn | info (=default)")
    parser.add_argument("--stdout-loglevel",type=str, help="Specify the stdout log level : error (=default) | warn | info")
    parser.add_argument("--upload-missing", help="Only upload missing local files.", action="store_true")

    args = parser.parse_args()

    JOB_CONF_FILE = args.configuration_file 
    DRYRUN = args.dryrun

    #log_level = args.loglevel
    file_loglevel = args.file_loglevel
    stdout_loglevel = args.stdout_loglevel

    if file_loglevel not in ["error", "warning", "info", "debug"]:
        file_loglevel = "info"
    if stdout_loglevel not in ["error", "warning", "info", "debug"]:
        stdout_loglevel = "error"

    xml_report = None
    logger = None
    if args.coreport:
        try:
            _function = "CO-01-34-08"
            _comment = "Synchronisation Copernicus DS"
    
            xml_report = xmlProcessReport.XmlReport()
            xml_report.begin(_function, _comment)
            logger = CmemsLogger(xml_report, stdout_loglevel=stdout_loglevel)
        except NameError:
            logger = CmemsLogger(stdout_loglevel=stdout_loglevel)
            logger.log_warning("No Coriolis report module found. Replacing with default logging.")
    else:
        logger = CmemsLogger(file_loglevel=file_loglevel, stdout_loglevel=stdout_loglevel)

    logger.log_info("Starting sync job")
    job = SyncJob(JOB_CONF_FILE, logger)

    job.sync_conf.rollback_file_path = "{}/{}-{}_{}-interrupted.lock".format(
        os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))),
        job.sync_conf.dataset["productId"],
        job.sync_conf.dataset["datasetId"],
        job.sync_conf.dataset["datasetVersion"]
    )

    if DRYRUN:
        job.dry_mode = True

    if args.upload_missing:
        action_process = Process(target=job.sync_only_missing_files)
    else:
        action_process = Process(target=job.run_sync)
    action_process.start()
    if job.sync_conf.transfer["timeout"] is not None:
        action_process.join(timeout=job.sync_conf.transfer["timeout"])
    else:
        action_process.join()
    action_process.terminate()

