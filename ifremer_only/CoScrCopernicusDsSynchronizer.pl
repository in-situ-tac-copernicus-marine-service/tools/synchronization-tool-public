#!/bin/perl
#****************************************************************************
#
# PRO : CORIOLIS
# MOD : CoScrCopernicusDsSynchronizer
# ROL : Synchronisation copernicus
#	
# CRE : 01/01/2018
# AUT : LF
# VER : @(#)$Revision: 34677 $
#       @(#)$Date: 2018-03-08 15:24:40 +0100 (jeu., 08 mars 2018) $
#
#****************************************************************************
# 
# HST : 11/01/2018 LF  FAE 39794 : Creation
#       22/01/2018 LF  FAE 39794 : Sous-repertoire en option
#       08/03/2018 AQ  FAE 40297 : Refonte globale synchronisation 
#
#****************************************************************************
#
# Copyright, IFREMER, 2018
#
#****************************************************************************

push( @INC, "xxx/tools/perl" );
require("CoScrReport.pl");
require("CoScrUtility.pl");
require("CoScrLauncher.pl");
use strict;
use warnings;


#****************************************************************************
# CONSTANTES
#****************************************************************************
my $TRUE  = 1;
my $FALSE = 0;

#****************************************************************************
# PARAMETRES 
# - $PYTHON_LAUNCHER : Name of the python binary (should be in the path of the conda environment
# - $PYTHON_SCRIPT : Name of the main python script
# - $PYTHON_OPTIONS : Options (--dryrun, --coreport, --file-loglevel, --stdout-loglevel, --upload-missing)
# - $CATEGORY : Stands for the name of the product/dataset to sync
#****************************************************************************

our $OPERATOR      = "";
our $CO_FUNCTION   = "CO-01-34-08";
our $CO_COMMENT    = "Synchronisation Copernicus DS";
our $LOCK_FILENAME = "co013408.xml";
our $LOCK_MODE     = $TRUE;
our $MAX_DURATION  = 360;

our $CATEGORY = undef;

our $PYTHON_LAUNCHER	= "python";
our $PYTHON_SCRIPT	= "xxxx/exe/CoScrCopernicusDsSynchronizerMain.py";
our $PYTHON_OPTIONS	= "--coreport --stdout-loglevel warning";

sub CoScrCopernicusDsSynchronizer
{
	my $argnum;
	my $l_command;
	my $l_tag;
	my $l_value;
	my @l_tags;
        my @l_values;
	my $l_summary = "";
        #---------------------------------------------------------------------------
        # Retreive python program options
        #---------------------------------------------------------------------------
	foreach $argnum (0 .. $#ARGV -1)
	{
		$PYTHON_OPTIONS = "$PYTHON_OPTIONS $ARGV[$argnum]";
	}

	$CATEGORY = $ARGV[-1];

	#---------------------------------------------------------------------------
        # Arguments must be >= 2
        #---------------------------------------------------------------------------

        if (scalar(@ARGV) < 2)

        {
                CoScrReport::Start($CO_FUNCTION, $CO_COMMENT, $OPERATOR, $LOCK_FILENAME, $LOCK_MODE, $CATEGORY, $MAX_DURATION);
		CoScrReport::AddValue("category", $CATEGORY);
                CoScrReport::SendError("Number of parameters incorrect. You must at least specify the configuration file absolute path and the function category.");
        }
        CoScrReport::Start($CO_FUNCTION, $CO_COMMENT, $OPERATOR, $LOCK_FILENAME, $LOCK_MODE, $CATEGORY, $MAX_DURATION);
	CoScrReport::AddValue("category", $CATEGORY);
        #---------------------------------------------------------------------------
        # Launch sync job
        #---------------------------------------------------------------------------

	$l_command = "$PYTHON_LAUNCHER $PYTHON_SCRIPT $PYTHON_OPTIONS";
	CoScrLauncher::Run($l_command, \@l_tags, \@l_values);

	$l_summary = "$CATEGORY : ";
	while (scalar(@l_tags) > 0)
        {
                $l_tag = shift(@l_tags);
                $l_value = shift(@l_values);
                if ($l_tag eq "error")
                {
                        CoScrReport::AddValue("synchronization_error", $l_value);
                }
                elsif ($l_tag eq "warning")
                {
                        CoScrReport::AddValue("synchronization_warning", $l_value);
                }
                elsif ( $l_tag eq "info" ) {
                        CoScrReport::AddValue( "synchronization_info", $l_value);
                }
                elsif ( $l_tag eq "nb_files_to_upload" ) {
                        CoScrReport::AddValue( "nb_files_to_upload", $l_value);
			$l_summary .= "nb_files_to_upload $l_value; ";
                }
                elsif ( $l_tag eq "nb_files_uploaded" ) {
                        CoScrReport::AddValue( "nb_files_uploaded", $l_value);
			$l_summary .= "nb_files_uploaded $l_value; ";
                }
                elsif ( $l_tag eq "nb_files_failed" ) {
                        CoScrReport::AddValue( "nb_files_failed", $l_value);
			$l_summary .= "nb_files_failed $l_value; ";
                }
                elsif ( $l_tag eq "nb_files_to_delete" ) {
                        CoScrReport::AddValue( "nb_files_to_delete", $l_value);
			$l_summary .= "nb_files_to_delete $l_value; ";
                }
		elsif ( $l_tag eq "nb_files_cancelled" ) {
                        CoScrReport::AddValue( "nb_files_cancelled", $l_value);
                        $l_summary .= "nb_files_cancelled $l_value; ";
                }
	}
	CoScrReport::AddValue("summary", $l_summary);
        CoScrReport::End($TRUE);
}

&CoScrCopernicusDsSynchronizer();
