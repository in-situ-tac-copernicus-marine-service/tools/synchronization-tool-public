# synchronization-tool

## Content description
The project architecture is:

- ifremer_only directory: Only used by Ifremer. Please ignore it.
- main directory: Contains the main python launcher as well as a configuration file template
- package directory: Contains the mds_insitu_sync package. This is the tool that will need to be installed in the conda environment
- mds_insitu_sync_env.yml: conda environment file

## Installation


### Prerequisites

Conda must be already installed on the platform.  

After installation, you must add conda-forge channel in which some dependencies are located :
```
conda config --add channels conda-forge
```

### Install mds_insitu_sync_env conda environment 

- Download the project
- Install the conda environment using mds_insitu_sync_env.yml file

```
conda env create -f /path/to/synchronization-tool-public/mds_insitu_sync_env.yml
```

- test to activate the environment
```
conda activate mds_insitu_sync_env
```

### Install mds_insitu_sync python package

Important: Don't forget to activate the conda environment before install or uninstall the mds_insitu_sync package  

Steps:

- Activate the conda environment

```
conda activate mds_insitu_sync_env
```

- Install the mds_insitu (by pointing the pacakge directory (the one where the setup.py file is present)

```
pip install /path/to/synchronization-tool-public/package
```

## Updates

### Updates of CMT and mds_insitu_sync packages

You can update copernicusmarine toolbox (CMT) package version and mds_insitu_sync version without performing a complete reinstallation

- Download new version of the mds_insitu_sync package then:

```
conda activate mds_insitu_sync_env
conda install -c conda-forge copernicusmarine=X.X.X

pip install /path/to/synchronization-tool-public-vX.X.X/package

```


## Run the script

- Copy the main script (mds_insitu_sync_main.py) and the configuration file wherever you want. 
- If needed, create the timestamp file ([See timestamp file section for more details](#timestamp-file))
- Correctly fill out the configuration file and create the needed directories ([See configuration section for more details](#configuration-file))
- run the script ([See main runner section for more details](#main-runner))

```
conda activate mds_insitu_sync_env
python /path/to/mds_insitu_sync_main.py --stdout-loglevel info --dryrun /path/to/conf/INSITU_GLO_PHY_TS_OA_NRT_013_002_cmems_obs-ins_glo_phy-temp-sal_nrt_oa_P1M_202211.ini

```

## Configuration file

### Basic rules
Use the sample file provided in /main/template.ini .     
For each synchronization job, you will have to provide a dedicated configuration file. You can name it as you prefer. We recommend to name it like following pattern :  
```
<productId>_<datasetId>_[<datasetSubPart>]_<datasetVersion>.ini
```

### Configuration sections

| Section | Item name | Is Mandatory |	Description |  Item example |
| ------ | ------ | ------ | ------ | ------ |
|MDS|ReadUserName|Yes|A username able to use the CopernicusMarineToolbox |xxxx|
|MDS|ReadUserPassword|Yes||xxxx|
|MDS|CacheDirectory|Yes|Local folder that contains the cache used by the CopernicusMarineToolbox|/path/to/cache|
|MDS|RemoteListingMethod|Yes| 3 values accepted: <br> - copernicusmarine_api: Use the copernicusmarine toolbox to list remote MDS files (recommended)<br> - direct_s3: Requests direct S3 URLs to list remote MDS files <br> - disabled: The script will not list remotes files. In this case, local files will be pushed but there will be no remote file deletion|copernicusmarine_api|
|FTP|WriteHost|Yes|FTP host where data are uploaded (DBS or DV)|nrt.cmems-du.eu|
|FTP|WriteUserName|Yes|A username accessing to FTP (with rw perms) where data are uploaded (DBS or DV)|xxxx|
|FTP|WriteUserPassword|Yes||xxxx|
|Dataset|PushingEntity|Yes|Pushing Entity filled in DNT file|CopernicusMarine-InSitu-Global|
|Dataset|ProductId|Yes|Full name of the product|INSITU_GLO_PHYBGCWAV_DISCRETE_MYNRT_013_030|
|Dataset|DatasetId|Yes|Full name of the dataset|cmems_obs-ins_glo_phybgcwav_mynrt_na_irr|
|Dataset|DatasetVersion|Yes|version of the dataset|202311|
|Dataset|DatasetPart|No|Subpart of a dataset (latest/monthly/history)|latest|
|Dataset|TimestampFile|No (yes if ListingMethod defined)|Absolute path to the timestamp file used for this synchronization job. (It is expected that this file is always accessible with rw permissions for the user executing the sync job)|/path/to/timestamp/timestamp.timestamp|
|Dataset|SourceDatasetPath|Yes|Absolute path to the data on your local disks. The path must refere to the dataset level. Never include dataset subpart (latest/monthly/history) here|/path/to/dataset|
|Dataset|SourceIndexDataFile|No|If an index_data file is to be uploaded, specify its absolute path|/path/to/index_file/index_latest.txt|
|Dataset|SourceIndexPlatformFile|No|If an index_platform file is to be uploaded, specify its absolute path|/path/to/index_file/index_platform.txt|
|Dataset|ListingMethod|Yes|At the moment, only "file" method is available. More options to come|file|
|Dataset|DateUpdateFromNetcdf|No|Deprecated: Please don't use this option|true or false (default = false)|
|Dataset|ExcludeRegex|No|Valid regular expression that matches the files you want to exclude from the synchronization|.*\\.nc\\.bak|
|Transfer|TmpDeliveryNotePath|Yes|Absolute path on your local disks where the application will write its temporary delivery notes (It is expected that this folder is always accessible with rw permissions for the user executing the sync job)|/path/to/dnt|
|Transfer|TmpCopyPath|Yes|Absolute path on your local disks where the application will copy the temporary index files and the temporary MDS remote listing file. (It is expected that this folder is always accessible with rw permissions for the user executing the sync job)|/path/to/tmp|
|Transfer|TransferLogFilePath|No|Absolute path on your local disks where the application will write its logs (It is expected that this folder is always accessible with rw permissions for the user executing the sync job)|/path/to/log|
|Transfer|ParallelTransfer|No|If true, it will make the program parallelize the FTP uploads. This can help greaty accelerate the uploads|true or false (default = false)|
|Transfer|ParallelTransferCount|No|The number of parallel uploads to launch. (max = 5)|4|
|Transfer|SendingAttempts|No|The number of attempts to make before considering the file upload as failed. (default = 1)|3|
|Transfer|Timeout|No|Maximum duration in minutes. (no default)|360|
|Transfer|SplitTransfer|No|Integer value expected. When set to n, the job will split the transfer into batches of n files. Each chunk will have its own DNT. It helps making the data available on FTP DU as soon as possible|500|
|Transfer|CheckIfFileOnDBS|No|Before sending a file on the write FTP host, check if it is already on the FTP, waiting for ingestion. In this case, bail out for this file and go on with other files. Helpful in the situation where onetransfer is not over and another sync job is starting, trying to push the same files|true or false (default = false)|



## Timestamp file
Be sure that the timestamp file declared in your configuration file exists and is accessible (read + write perms) for your sync user.  

Before launching your first job, for each dataset, you will have to create this file and insert into it a timestamp that will be the reference for the first synchronization. This timestamp must stand for a datetime that is prior to all your files to synchronize.  

For a first run, you can edit your timestamp file and set 0 in the file.  
Alternatively, you can use thr following command (modify the timestamp file path accordingly to what you specified in your configuration file) :
```
echo `date -d "1971-01-01 00:00:00" +%s` > <PATH TO YOUR TIMESTAMP FILE>
```

## Functional specifications

### Main steps:
- <b>Manage job timestamps</b>: mds_insitu_sync starts by reading the "last job datetime" file to retrieve the timestamp of the last job execution. It also records the timestamp of the current job execution in memory for use later in step 6
- <b>Local files listing</b>: mds_insitu_sync lists the files on the local file system with their latest modification times.
- <b>Remote files listing</b>: mds_insitu_sync retrieves the list of files on the remote file system (MDS) using the "get" method from the Copernicus Marine Toolbox (Python API) or by requesting directly S3 URLs.
- <b>Creation, updates and deletions detection</b>: mds_insitu_sync compares the list of remote and local files to generate three lists:
  - <b>List of new local files</b>: Files available locally but not on the MDS, which need to be pushed to the DV.
  - <b>List of files to delete</b>. Files available on the MDS but not locally, which need to be deleted from the MDS.
  - <b>List of updated local files</b>. Files that exist both locally and on the MDS, where the local version has a newer modification time than the timestamp of the last job execution retrieved in step 1. These files need to be pushed to the DV.
- <b>Files upload</b>:
  - mds_insitu_sync pushes local files and Delivery Note Texts (DNT) for ingestions and deletions. The tool allows for transferring files in batches. In other words, a single DNT file can be pushed for every x files sent (with x configurable in the tool) in order to optimise the Delivery Validator processing actions.
  - If the index files are available, mds_insitu_sync also pushes them and their DNT to the Delivery Validator.
- <b>Sync job ended</b>: mds_insitu_sync updates the "last job datetime" file with the timestamp recorded at the beginning of the current job in step 1. This updated timestamp will be used for comparison during the next job execution.

![Workflow diagram](./steps_schema.png)

### New dataset not yet available on the MDS (but available on the DV)
Before an official EIS date, when a new dataset is created on the DV but does not yet exist on the MDS catalog. You have two options:
- Set RemoteListingMethod = direct_s3. The new dataset is accessible by requesting S3 URLs directly. Using this method, the remote files can be listed as usual.
- Set RemoteListingMethod = disabled. This ensures that the synchronization tool will only push data to the DV without trying to list the remote files on the MDS. In this case, no remote deletion will be manage. If you use this option, don't forget to change this RemoteListingMethod at the EIS date.

### Additionnal informations
- For each file delivered, a checksum will be computed using the md5 algorithm, in order to ensure the proper synchronisation between the PUs and the MDS 
- The mds_sync_tool includes an automatic retry mechanism for handling file transfer failures. If a file transfer fails, the tool will attempt to retry the transfer up to five times. If the transfer still fails after five attempts, an error is logged. 
- The DNT is uploaded to the "/product/DNT" folder on the Delivery Validator 



## Main runner

Basically, you launch a sync job with following command :
```
conda activate mds_insitu_sync_env
python /path/to/main/mds_insitu_sync_main.py /path/to/conf/INSITU_GLO_PHY_TS_OA_NRT_013_002_cmems_obs-ins_glo_phy-temp-sal_nrt_oa_P1M_202211.ini

```

You can also add following options :
- --file-loglevel "level" : one of (error, warning, info, debug) ; default = info
- --stdout-loglevel "level" : one of (error, warning, info, debug) ; default = error
- --dryrun : only print actions & create dnt files on local side.
- --upload-missing : Only upload missing local files



## Some issues

### SSLError
Issue example:
```
HTTPSConnectionPool(host='stac.marine.copernicus.eu', port=443): Max retries exceeded with url: /mdsVersions.json?x-cop-client=copernicus-marine-client (Caused by SSLError(SSLError(5, '[SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:1007)')))
```

- Check if the variable REQUESTS_CA_BUNDLE is active in your environnement variables.
   -  If active, unset it

- Is issue still here?
  - If yes: copy the system ca-certificates.crt file to the conda environment and rename it to cacert.pem (and remember: REQUESTS_CA_BUNDLE should never be set)


### no running event loop
The version 3.10.0 of aiohttp breaks the copernicusmarine toolbox for versions below 1.3.2 (no running event loop issue )

Recently, when we create a conda environnement using the mds_insitu_sync_env.yml file and when copernicusmarine version is 1.2.3, the aiohhtp package was updated from version 3.9.X to 3.10.X.  
Solution : Update the copernicusmarine toolbox to 1.3.4 version
